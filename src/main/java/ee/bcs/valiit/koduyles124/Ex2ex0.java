package ee.bcs.valiit.koduyles124;

public class Ex2ex0 {
    public static void main(String[] args) {

        System.out.println("Hello world!");
        System.out.println("\"Hello world!\"");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren't funnny\".");
        System.out.println("\"See on teksti esimene pool" + " See on teksti teine pool\"");
        System.out.println("Elu on ilus");
        System.out.println("Elu on 'ilus'");
        System.out.println("Elu on \"ilus\"");
        System.out.println("Kõige rohkem segadust tekitab \" - märgi kasutamine sõne sees");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis");
    }
}
