package ee.bcs.valiit.koduyles124;

public class Ex2ex3 {

    public static void main(String[] args) {
        String
                planet1 = "Merkuur",
                planet2 = " Veenus",
                planet3 = " Maa",
                planet4 = " Marss",
                planet5 = " Jupiter",
                planet6 = " Saturn",
                planet7 = " Uran",
                planet8 = " Neptuun";

        System.out.println('"' + planet1 + ',' + planet2 + ',' + planet3 + ',' + planet4 + ',' + planet5 + ',' + planet6 + ',' + planet7 + ',' + planet8 +" on Päikesesüsteemi 8 planeeti\"");

        int planetCount = 8;

        System.out.println(
                String.format(
                        "%s,%s,%s,%s,%s,%s,%s,%s on Päikesesüsteemi %s planeeti",
                        planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8,
                        planetCount
                )
        );

    }
}
