package ee.bcs.valiit.koduyles124;

public class Ex4ex3 {

    public static void main(String[] args) {
        int a = 5;
        if (a < 2) {
            System.out.println("Nõrk");
        } else if (a < 3) {
            System.out.println("Mitterahuldav");
        } else if (a < 4) {
            System.out.println("Rahuldav");
        } else if (a < 5) {
            System.out.println("Hea");
        } else if (a < 6) {
            System.out.println("Väga hea");
        }
    }
}
