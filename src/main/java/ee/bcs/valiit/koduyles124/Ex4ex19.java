package ee.bcs.valiit.koduyles124;

import java.util.ArrayList;

public class Ex4ex19 {

    public static void main(String[] args) {

        ArrayList<String> linnad = new ArrayList<String>();

        linnad.add("Tallinn");
        linnad.add("Tapa");
        linnad.add("Tartu");

        System.out.println(linnad.get(0));
        System.out.println(linnad.get(2));
    }

}
