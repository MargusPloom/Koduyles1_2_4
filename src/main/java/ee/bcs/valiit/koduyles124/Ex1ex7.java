package ee.bcs.valiit.koduyles124;

public class Ex1ex7 {

    public static String deriveBirthYear(String isikukood) {
        String aasta = isikukood.substring(1, 3);
        String sajand = "";
        char sugu = isikukood.charAt(0);
        switch (sugu) {
            case '1' : sajand = "18"; break;
            case '2' : sajand = "18"; break;
            case '3' : sajand = "19"; break;
            case '4' : sajand = "19"; break;
            case '5' : sajand = "20"; break;
            case '6' : sajand = "20"; break;
            default: break;


        }
        return sajand + aasta;
    }

    public static void main(String[] args) {
        String aasta = deriveBirthYear("36808230220");
        System.out.println(aasta);
    }
}
