package ee.bcs.valiit.koduyles124;

public class Ex4ex5 {

    public static void main(String[] args) {

        int vanus = 100;
        String resultaat;

        resultaat = (vanus > 100) ? "Vana" : "Noor";

        System.out.println(resultaat);
    }
}
