package ee.bcs.valiit.koduyles124;

public class Ex2ex2 {

    public static void main(String[] args) {
        String bookTitle = ("Rehepapp");

        String kirjanik1 = "Raamatu \"" + bookTitle + "\" autor on Andrus Kivirähk.";

        StringBuilder kirjanik2 = new StringBuilder();
        kirjanik2.append("Raamatu ");
        kirjanik2.append('"');
        kirjanik2.append(bookTitle);
        kirjanik2.append('"');
        kirjanik2.append(" autor on Andrus Kivirähk.");

        String kirjanik3 = String.format("Raamatu %c%s%c autor on Andrus Kivirähk.", '"', bookTitle, '"');

        System.out.println(kirjanik1);
        System.out.println(kirjanik2);
        System.out.println(kirjanik3);

        System.out.println("Raamatu \"" + bookTitle + "\" autor on Andrus Kivirähk.");
    }
}
