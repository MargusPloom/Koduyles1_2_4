package ee.bcs.valiit.koduyles124;

import java.util.HashMap;
import java.util.Map;

public class Ex4ex22 {

    public static void main(String[] args) {
        Map<String, String[]> map = new HashMap<>();
        String[] estCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        map.put("Estonia", estCities);
        String[] sweCities = {"Stockholm", "Uppsala", "Lund", "Köping"};
        map.put("Sweden", sweCities);
        String[] finCities = {"Helsinki", "Espoo", "Hanko", "Jämsä"};
        map.put("Finland", finCities);



        map.forEach((k, v) -> {
            System.out.println("Country: " + k);
            System.out.println("Cities: ");
            for (String city : v) {
                System.out.println("\t" + city);
            }

        });

    }

}