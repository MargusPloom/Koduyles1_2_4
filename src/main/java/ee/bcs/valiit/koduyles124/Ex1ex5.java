package ee.bcs.valiit.koduyles124;

public class Ex1ex5 {

    public static String deriveGender(String isikukood) {

        char sugu = isikukood.charAt(0);
        switch (sugu) {

            case '3':
                return "M";
            case '4':
                return "F";

            default:
                return "Viga sisestamisel";

        }

    }

    public static void main(String[] args) {
        String gender = deriveGender("66808230220");
        System.out.println(gender);
    }
}