package ee.bcs.valiit.koduyles124;

import java.util.TreeSet;

public class Ex4ex21 {

    public static void main(String[] args) {

        TreeSet<String> nimed = new TreeSet<>();

        nimed.add("Juku");
        nimed.add("Ats");
        nimed.add("Ants");
        nimed.add("Karl");
        nimed.add("Jaana");
        nimed.add("Jaak");

        int i = 0;
        for (String nimi : nimed) {

            System.out.println(nimi);
        }

    }
}



